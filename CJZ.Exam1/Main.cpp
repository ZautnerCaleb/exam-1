#include <iostream>
#include <conio.h>

using namespace std;

//Perform the Square operation
void Square(float &num) {
	num = num * num;
}

//Perform the Cube operation
void Cube(float &num) {
	num = num * num * num;
}

//After operational input, this runs through the calculation process
void Calculation(float &num, int math) {
	if (math == 2) {
		Square(num);
	}
	else if (math == 3) {
		Cube(num);
	}
	//If neither square or cube, repeat input
	else {
		cout << "Invalid input. Please enter 2 for Square or 3 for Cube: ";
		cin >> math;
		Calculation(num, math);
	};
};

int main() {

	float num;
	char repeat;

	do {

		int math;

		cout << "Please enter a number: ";
		cin >> num;

		cout << "Would you like to square or cube the number? 2 for Square, 3 for Cube: ";
		cin >> math;

		Calculation(num, math);		

		cout << "\n" << "The answer is " << num << "\n" << "\n";
		cout << "Would you like to perform another opertation? Y for yes, N for no: ";
		cin >> repeat;
		cout << "\n";

	} while (repeat == 'Y'||repeat == 'y');


	_getch();
	return 0;

}